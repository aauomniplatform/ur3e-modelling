%%----------- ROS Node controller -----------
%% Set up
%Add dependencies
clear
fprintf('Loading modell and trajectory...');
addpath('functions')
addpath('ros')
addpath('model')
load('UR3e_param.mat');
load('eeConfig.mat');

% Start variables
% Plan trajectory from A to B
% Desired trajectory points and parameters
waypoints = [ 0.50  0.05  0.07; 
              0.40  0.15  0.10; 
              0.35  0.30  0.10; 
              0.30  0.40  0.10; 
              0.20  0.45  0.10; 
              0.20  0.45  0.10];
n = 10;
t_init = 0;
t_end = 10;
% Cartesian-space trajectory
[xr, dxr, d2xr, t1] = CTrajectory(waypoints,t_init,t_end, n);
% Joint-space trajectory:
[qr, dqr, d2qr, t2] = JTrajectory(waypoints,t_init,t_end, solver, 'gripper_link', eeRight, zeros(6,1));
% Start variables
dx = 0; x_old=0; sim_time = 0;
fprintf(' UR3e and waypoints ready.\n');

% Plotting
figure 
%imshow('plottingQ.jpg')
prompt = 'Enable robot plotting? [Y/N]: ';
plotEN = 0;
while(1)
    str_plot = input(prompt,'s');
    if ((str_plot)=='Y')
        plotEN=1;
        break
    end
    
    if ((str_plot)=='N')
        fprintf('Plotting is not enabled.\n');
        break
    end
end
close all

if(plotEN==1)
    figure
    show(UR3e, zeros(6,1),'Frames','off','PreservePlot',false);
    hold all
    grid on
    plot3(xr(:,1),xr(:,2),xr(:,3),'-r','LineWidth', 2);
    plot3(waypoints(:,1),waypoints(:,2),waypoints(:,3),'o','LineWidth', 2);
    title('Robot position')
    xlabel('x [m]')
    ylabel('y [m]')
    zlabel('z [m]')
    axis([-0.6 0.6 -0.6 0.6 -0.6 0.6])
end
% Connect to ROS master
rosshutdown;
ROSIp = '192.168.87.106';
rosinit(ROSIp);
[ConPub,ConMSG] = rospublisher('/URinput');
ConSub = rossubscriber('/URfeedback');

%% Loop
i = 1;
limit = length(t1);
fprintf('Node is active now.\n');
while(1)
% Get Robot feedback
    % read ROS Manipulator
    receive(ConSub,10); 
    msgSub = ConSub.LatestMessage.Data;
    %fprintf('q: %s\n', msgSub);   %<- use this to see what you're getting
    % read current simulation time
     
    
    % check conversion data from string to num
    %q = msgSub;
    % the message must be in the format: msgSub = '1 2 3 4 5 6';
    [q,sucess] = str2num(msgSub);
    sizeq = size(q,2); %<- vector size must be 1x6
    
    if (sucess && (sizeq==6))
        % cmmd window and plot
        %fprintf('Joint q values received.\n');
        
        if(plotEN==1)
        show(UR3e, q','Frames','off','PreservePlot',false);
        end
        
        % Controller 
            % Transform from joint to cartesian -space (direct kinematics)
             transform = getTransform(UR3e,q','base_link','gripper_link');
             x = tform2trvec(transform);
             dx = x-x_old;

             % select reference according time
             if (i<limit)
             t_i = t1(i);
             xr_t = xr(i,:);
             dxr_t = dxr(i,:);
             end
             % Position controller
             forceU = Cposcontroller(UR3e,xr_t,dxr_t,x, dx,q, 'gripper_link');
        % 
             % If the robot is force actuated:
             %input = forceU;
             input=sprintf('%7.4f,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f',forceU(1),forceU(2),forceU(3),forceU(4),forceU(5),forceU(6));
        % Send Robot input
            % Write on ROS Manipulator
            ConMSG.Data = input;
            send(ConPub,ConMSG); 

        % Update values for nex iteraction
            x_old = x;
            %sim_time = sim_time+1;    
            i = i + 1;
    end
end

%% end
hold off
close all
rosshutdown;
clc