%% Set up
% Connect to ROS master
rosshutdown;
ROSIp = '192.168.87.106';
rosinit(ROSIp);
% If MATLAB connected properly to the ROS master, it should return:
% Initializing global node /matlab_global_node_45999 with NodeURI http://patmos-ThinkPad-T450s:34353/
%send(pub,msg)
% Create ROS subscribers and publishers
[Pub,msgPub] = rospublisher('/chatter');
i = 1;
%% Loop
while(1) 
   %talker
   msgPub.Data = sprintf('hello world %d', i);
   send(Pub,msgPub);   
   pause(0.05);
   i = i+1;
end
rosshutdown;