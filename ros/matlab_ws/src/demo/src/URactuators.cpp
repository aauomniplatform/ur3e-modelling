#include "ros/ros.h"
#include "std_msgs/String.h"

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("Force: %s", msg->data.c_str());
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "URactuators");
  ros::NodeHandle n;

// %Tag(SUBSCRIBER)%
  ros::Subscriber sub = n.subscribe("URinput", 1000, chatterCallback);

// %Tag(SPIN)%
  ros::spin();
  return 0;
}
