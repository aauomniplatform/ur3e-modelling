#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>
int main(int argc, char **argv)
{

  ros::init(argc, argv, "URsensors");
  ros::NodeHandle n;
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("URfeedback", 1000);
// %EndTag(PUBLISHER)%

// %Tag(LOOP_RATE)%
  ros::Rate loop_rate(10);

  int count = 0;
  while (ros::ok())
  {
    std_msgs::String msg;

    std::stringstream ss;
    ss << "3.1309 3.1241 -0.0000 3.6940 3.1325 -2.6067"; //<< count;
    msg.data = ss.str();

    ROS_INFO("%s", msg.data.c_str());

// %Tag(PUBLISH)%
    chatter_pub.publish(msg);
// %Tag(SPINONCE)%
    ros::spinOnce();
// %Tag(RATE_SLEEP)%
    loop_rate.sleep();
// %EndTag(RATE_SLEEP)%
    ++count;
  }
  return 0;
}
