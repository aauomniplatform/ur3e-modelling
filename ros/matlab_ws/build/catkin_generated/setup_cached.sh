#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export PKG_CONFIG_PATH="/home/carol/catkin_ws/devel/lib/pkgconfig:/opt/ros/melodic/lib/pkgconfig"
export PWD="/home/carol/LH8/ur3e-modelling/ros/matlab_ws/build"
export ROS_PACKAGE_PATH="/home/carol/LH8/ur3e-modelling/ros/matlab_ws/src:/home/carol/catkin_ws/src:/opt/ros/melodic/share"