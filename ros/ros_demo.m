%% Set up
% Connect to ROS master
rosshutdown;
%ROSIp = '';
ROSIp = '192.168.87.106';

rosinit(ROSIp);
% If ROS Master was properly started, it should return something like:
% The value of the ROS_MASTER_URI environment variable, http://localhost:11311, will be used to connect to the ROS master.
% Initializing ROS master on http://patmos-ThinkPad-T450s:11311/.
% Initializing global node /matlab_global_node_89868 with NodeURI http://patmos-ThinkPad-T450s:37735/

% Create ROS publishers and suscribers
[talker,talker_msg] = rospublisher('/chatter');
listener = rossubscriber('/chatter');
i = 1;

%% Loop
while(1) 
    
   %talker
   talker_msg.Data = strcat('hello world ', string(i));
   send(talker,talker_msg); 
   
   %listener  
   receive(listener,10); 
   listener_msg = listener.LatestMessage.Data;
   fprintf('I heard: %s\n', listener_msg);        
end
rosshutdown;