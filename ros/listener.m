%% Set up
% Connect to ROS master
rosshutdown;
ROSIp = '192.168.87.106';
rosinit(ROSIp);
% If MATLAB connected properly to the ROS master, it should return:
% Initializing global node /matlab_global_node_45999 with NodeURI http://patmos-ThinkPad-T450s:34353/

% Create ROS subscribers and publishers
imgSub = rossubscriber('/chatter');

%% Loop
while(1) 
   receive(imgSub,10); 
   msgSub = imgSub.LatestMessage.Data;
   fprintf('I heard: %s\n', msgSub);        
end
rosshutdown;