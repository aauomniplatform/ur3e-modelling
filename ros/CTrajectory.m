function [xr, dxr, d2xr, t] = CTrajectory(waypoints,t_init,t_end, n)
% Full Cartesian-space trajectory generator
%[x, dx, d2x, t] = CTrajectory(waypoints,t_init,t_end, n)
% Input parameters:
%- waypoints: Nx3 matrix, which every row is a point with the format [x y
%z].
%- t_init: trajectory start time.
%- t_end: trajectory finish time.
%- n: amount of middle points interpolated between each point.
% Return parameters:
%- x, dx, d2x: reference position, velocity and acceleration.
%- t: time vector for every position, velocity and acceleration component.
%
% Note: this will generate a simple linear trajectory between 2 waypoints.
% For multiple waypoints it will generate a natrual curve between all of them.

% Interpolate cartesian points with a smooth curve
Path = GeneratePath(n,waypoints,'with');
sizeP = size(Path,1);
controlPoints = Path';

% Obtain time vector
t_temp = zeros(1,sizeP);
if (sizeP==1)
    fprintf("Trajectory too small.\n");
else
    deltaT = (t_end-t_init)/(sizeP-1);

    for i = 1:sizeP(1)
        t_temp(i)=t_init+deltaT*(i-1);
    end
end

% Generate complete trajectory
tInterval = [t_init t_end];
tSamples = t_temp;
[q,qd,qdd,pp] = bsplinepolytraj(controlPoints,tInterval,tSamples);

xr = q';
dxr = qd';
d2xr = qdd';
t = t_temp;
end