function [forceU] = Cposcontroller(robot,xr,dxr,x, dx,q, eeName)
% Cartesian position controller for force-actuated manipulators
%- robot: input RigidBodyTree.
%- xr,dxr: reference position and velocity vectors 1x3 in cartesian space.
%- x, dx: current position and velocity in cartesian space.
%- q: current joints position vector 1x6.
%- forceU: returned force vector 1x6 to be applied on the manipulator.

% error and gains feedback
Kp = 1; 
Kd = 1;
e = xr - x; de = dxr - dx;
PD = (Kp*e + Kd*de)';

% feedback control loop: F(q) = J(q)^T * F(x,y,z)
Torquesr = [0; 0; 0];
Fx = [PD; Torquesr];
Jt = (geometricJacobian(robot,q',eeName))';
feedback = Jt*Fx;

% feedforward: gravity compensation
gravTorq = gravityTorque(robot,q');

forceU = (feedback+gravTorq)';
end