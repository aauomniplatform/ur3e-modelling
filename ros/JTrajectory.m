function [qr, dqr, d2qr, t] = JTrajectory(waypoints,t_init,t_end, solver, eeName, effector, Joint_init)
% Full Cartesian-space trajectory generator
%[qr, dqr, d2qr, t] = JTrajectory(waypoints,t_init,t_end, solver, eeName, effector, Joint_init)
% Input parameters:
%- waypoints: Nx3 matrix, which every row is a point with the format [x y
%z].
%- t_init: trajectory start time.
%- t_end: trajectory finish time.
%- solver: manipulator inverse kinematics.
%- eeName: end effector link name.
%- effector: end effector position during the trajectory.
%- Joint_init: initial joint position
% Return parameters:
%- q, dq, d2q: reference position, velocity and acceleration.
%- t: time vector for every position, velocity and acceleration component.

% Manipulator parameters
weights = [1 1 1 1 1 1];
JointPath = GenerateJointPath(effector,waypoints,Joint_init,solver,weights,eeName);

% Interpolate Joint-points with a smooth curve
sizeP = size(JointPath,1);
controlPoints = JointPath';

% Obtain time vector
t_temp = zeros(1,sizeP);
if (sizeP==1)
    fprintf("Trajectory too small.\n");
else
    deltaT = (t_end-t_init)/(sizeP-1);

    for i = 1:sizeP(1)
        t_temp(i)=t_init+deltaT*(i-1);
    end
end

% Generate complete trajectory
tInterval = [t_init t_end];
tSamples = t_temp;
[q,qd,qdd,pp] = bsplinepolytraj(controlPoints,tInterval,tSamples);

qr = q';
dqr = qd';
d2qr = qdd';
t = t_temp;
end