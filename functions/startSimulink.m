function [SimCartesian, SimJoint, SimTime] = startSimulink(time,Path,JointPath)
% This function start the Simulink model variables and it adapts it to its
% format.
%[SimCartesian, SimJoint, SimTime] = startSimulink(time,Path,JointPath)
%- time: time vector 1x2 with initial and end time.
%- Path: cartesian-space trajectory.
%- JointPath: joint-space trajectory
t_init = time(1);
t_end = time(2); 
size_JP = size(JointPath);
t_temp = zeros(1,size_JP(1));

if (size_JP(1)==1)
    fprintf("Joint Path too small.\n");
else
    deltaT = (t_end-t_init)/(size_JP(1)-1);

    for i = 1:size_JP(1)
        t_temp(i)=t_init+deltaT*(i-1);
    end
end

Path_temp = Path(:,1:3);
SimCartesian = Path_temp';
SimJoint = JointPath';
SimTime = t_temp;

open('main_simulink.slx')
end