%------------ MAIN CODE FOR MODELLING -------------
clear
clc
%Change this command to your repository address. 
%cd 'C:\Users\cagom\Desktop\Aalborg Universitet\2nd Semester\Semester Projekt\Matlab stuff\UR3e-modelling'
addpath('functions')
addpath('model')
%% Load the Robot

%If the .urdf file was changed, uncomment the following lines:
%[UR3e, solver] = initialization_UR3e('ur3e.urdf')
%save('UR3e_param.mat', 'UR3e', 'solver');
%save('ur3ew_param.mat', 'ur3ew', 'solverw');
%Otherwise
load('UR3e_param.mat');
load('eeConfig.mat');

%% Generate a Set of Cartesian-Space Waypoints
% Structure of WayPoints matrix: 
%[x0 y0 z0;
% x1 y1 z0; ...
% xn yn zn];
Waypoints = [ 0.50  0.05  0.07; 
              0.40  0.15  0.10; 
              0.35  0.30  0.10; 
              0.30  0.40  0.10; 
              0.20  0.45  0.10; 
              0.20  0.45  0.10];
n = 10;
Path = GeneratePath(n,Waypoints,'with');
size_Path = size(Path);

%% Convert Cartesian-Space Waypoints to Joint-Space Using Inverse Kinematics
% Define the orientation so that the end effector is oriented down
weights = [1 1 1 1 1 1];
eeName = 'gripper_link';
Joint_init = zeros(6,1);
effector = eeRight;
JointPath = GenerateJointPath(effector,Path,Joint_init,solver,weights,eeName);
% Visualisation:
PlotTrajectory(Waypoints, Path, JointPath);

%% Follow trajectory in Matlab
% This is how the robot is supposed to move and follow the trajectory
PlotFollow(UR3e, Waypoints, Path, JointPath, 0);
%% Send data to Simulink variable(s)
%Time interval given in [s]
t_init = 0;
t_end = 10; 
%[SimCartesian, SimJoint, SimTime] = startSimulink([t_init t_end],Path,JointPath);

%% Controller development and test in Simulink
% this script loads an example trajectory and exports it to Simulink
simulink;
% do not run this section in case of already using the previous script in 
% case of already working with the previous part of the script

%% Controller development in Webots
% this script loads an example trajectory and prints the controller on .c
webots;
% do not run this section in case of already using the previous script in 
% case of already working with the previous part of the script

%% Modify end effector configuration
%Choose an end-effector contiguration:
% eeDown=[1 0 0 pi];      % gripper pointing down  
% eeLeft=[1 0 0 pi/2];    % gripper pointing to the left (world -Y)
% eeRight=[-1 0 0 pi/2];  % gripper pointing to the right (world +Y)
% eeFront=[0 1 0 pi/2];   % gripper pointing towards (world +X)
% eeBack=[0 -1 0 pi/2];   % gripper pointing inward (world -X)
% eeUp=[0 0 1 pi/2];      % gripper pointing up
%save('eeConfig.mat','eeDown','eeLeft','eeRight','eeFront','eeBack','eeUp');