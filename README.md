# README #

This is a repository with all the MATLAB files used for the modelling of the UR3e robot.
It is included in the Little Helper 8 project, by Aalborg University.

![](info/ur3e_workspace.png)

![](info/environment_gif.gif)


## 1. MATLAB as trajectory generator and robot importer ##

Set up in MATLAB the same directory as this repository.

* In case of further controller development, run the simulink.m script and work on ur3e_controller.slx.
* For further development Matlab-Webots, run the webots.m script and work on the "webots_ur3e" webots project directory.
* In case of having already a controller, run main.m, and import the controller into main_simulink.slx.

It is possible to see each trajectory in Matlab before loading it into Simulink.

![](info/trajectory_gif.gif)


#### Modifying the robot model ####

For new trajectories:


1. Change the waypoints in the main file.
2. Rerun the trajectory generation.

For new models:


* If you edit the .urdf file, you have to re-import it to MATLAB and Simscape, so both models match.

#### Importing a robot from ROS to MATLAB ####

1. Convert main body tree from xacro to URDF using ROS commands. Also delete not-readable fields from the URDF file.
2. Edit visual fields from the URDF file, because the mesh files are referred as an address link, instead of directly referring the visual field. In this case, the URDF only contains the compatible .stl files.
3. Move the visual and URDF files into the MATLAB workspace. In this case, a sub-folder was made to contain them, including the gripper.
4. Once the model is ready, it can be finally imported into the workspace and visualize it.


## 2. Simulink as simulation platform ##

* It is possible visualise the complete smooth joint trajectory, among the velocity and acceleration.
* The dynamics are calculated in the UR3e Simscape model.
* In ur3e_controller.slx the robot is force-actuated, for easier controller improvement.
* In main_simulink.slx is also the trajectory generators included, but the controller is not fully set yet.
* By running the simulation, you can also see the robot moving in SimMechanics.

![](info/simscape_example.png)

![](info/simscape_gif.gif)

#### Controller development ####

![](info/simulink_overview.png)

1. There a currently two controllers in development for the UR3e. 
One position controller in Cartesian-space for a force-actuated arm (main_simulink.slx).
One force controller on the end effector for a motion-actuated arm (ur3e_controller.slx).
2. Both controllers are in separated files and can be develop independently.
3. Current approach is using PIDs.


## 3. MATLAB as a node in ROS ##

The files for this purpose are located in the ''ros'' subfolder. There is a catkin workspace inside, which contains the a ROS package called demo.
MATLAB can either connect to ROS or start its own master. In case of using its own master, other ROS nodes outside MATLAB can connect to it as well.

For the controller implementation, the script ConNode.m shall be developed as the main controller node for the project. Extra functions have been created to work as the trajectory generator and the feedback-controller from Simulink.
Finally, to emulate the robot there are two nodes on the package: URsensors (publishes the joint positions) and URactuators (suscribres to the joint commands), that can be further improved for simulation.

![](info/package.png)

![](info/controller_node.gif)


#### How to start the node ####

**# Terminal 1**

	cd ~/repository_location/LH8/ur3e-modelling/ros/matlab_ws
	catkin_make
	source devel/setup.bash
	roscore

**# Terminal 2**

	cd ~/repository_location/LH8/ur3e-modelling/ros/matlab_ws
	source devel/setup.bash
	rosrun demo URsensors

**# Terminal 3**

	cd ~/repository_location/LH8/ur3e-modelling/ros/matlab_ws
	source devel/setup.bash
	rosrun demo URactuators

**# MATLAB**

1. Check the IP address where the ROS master is running.
2. Run the ConNode script.
3. It is possible to choose if the ConNode plots the robot position as well.


#### Other files #####

For testing the ROS toolbox, extra files have been added.The talker and listener nodes from the ROS tutorials have been re-written as MATLAB nodes and sucessfully tested, so they have been included as example for this MATLAB demo.
To start these nodes, follow the same procedure as mentioned in the previous section.

![](info/talker.gif)

![](info/rosmaster.gif)


## 4. Miscellaneous ##

#### Exporting controller to Webots ####

This is still in progress. 
At the moment, it is possible to export the Joint-space trajectory into a .c file, which moves the UR3e robot in a small Webots world.
It is expected to export the whole force control into a .c or .py file compatible with the ROS LH8 architecture.

![](info/webots_gif.gif)

#### Comments #####

* You can use the help command on custom functions in Matlab for examples and further parameters explanation.
* Kinematics and Dynamics parameters for UR manipulators: https://www.universal-robots.com/articles/ur-articles/parameters-for-calculations-of-kinematics-and-dynamics/

#### Requisites ####

* MATLAB & Simulink, version 2019b or later.
* Add-ons: Robotics, Control Design, Simscape, Contact libraries, ROS, Curve-Fitting toolboxes.

-----------------------------------------------
## Authors

Ditte Damgaard Albertsen:	        dalber16@student.aau.dk

Rahul Ravichandran:			rravic19@student.aau.dk

Rasmus Thomsen:				rthoms16@student.aau.dk

Mathiebhan Mahendran:			mmahen15@student.aau.dk

Alexander Staal:			astaal16@student.aau.dk

Carolina Gomez Salvatierra:             cgomez19@student.aau.dk

---------------------------------------------------------
