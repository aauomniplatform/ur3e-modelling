%% Load controller development in Simulink
% uncomment the clear command in case of working exclusively here 
%clear 
addpath('functions')
addpath('model')
load('UR3e_param.mat');
FileName = 'Simulink_develop.mat';

%In case you need to reload the trajectory
%CreateExample(FileName, UR3e, solver) 
load(FileName);

PlotFollow(UR3e, Waypoints, Path, JointPath, 0)
fprintf('If you want to see the trajectory execution on Matlab again: \n');
fprintf('PlotFollow(UR3e, Waypoints, Path, JointPath, 0)\n');

open('ur3e_controller.slx')