% Webots
% uncomment the clear command in case of working exclusively here 
clear 
addpath('functions')
addpath('model')
addpath('webots_UR3e/controllers/UR3e_c')
load('ur3ew_param.mat'); %be careful with modifying this model
load('eeConfig.mat');
% example waypoints
Waypoints = [ 0.20  0.25  0.10; 
              0.20  0.35  0.10;
              0.20  0.25  0.20; 
              0.20  0.25  0.25];
% generate trajectory in cartesian-space
n = 5;
Path = GeneratePath(n,Waypoints,'without');
size_Path = size(Path);
% generate trajectory in joint-space
weights = [1 1 1 1 1 1];
eeName = 'wrist_3_link';
Joint_init = zeros(6,1);
effector = eeRight;
JointPath = GenerateJointPath(effector,Path,Joint_init,solverw,weights,eeName);
% print controller in .c format
PrintController(JointPath, 'UR3e_c')
% optional: show trajecotry in Matlab
PlotFollow(ur3ew, Waypoints, Path, JointPath, 0)

fprintf('If you want to see the trajectory execution on Matlab again: \n');
fprintf('PlotFollow(ur3ew, Waypoints, Path, JointPath, 0)\n');