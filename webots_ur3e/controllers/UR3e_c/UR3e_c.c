#include <webots/distance_sensor.h>
#include <webots/motor.h>
#include <webots/position_sensor.h>
#include <webots/robot.h>

#include <stdio.h>

#define TIME_STEP 32

// Global variables:
double target_positions[] = {0, 0, 0, 0, 0, 0};
double speed = 1.0;
  
/* function returning the vector */
void setArray(double array[6]) {
   int i = 0;
   for (i = 0; i < 6; ++i)
    target_positions[i]=array[i]; 
}

int main(int argc, char **argv) {
  wb_robot_init();
  float error_margin = 0.0001, error[6]={1, 1, 1, 1, 1, 1};
  int i = 0;
  int pointer = 0;
  bool flag = false;
// UR3 components
  WbDeviceTag ur_motors[6];
  ur_motors[1] = wb_robot_get_device("shoulder_lift_joint");
  ur_motors[2] = wb_robot_get_device("shoulder_pan_joint");
  ur_motors[0] = wb_robot_get_device("elbow_joint");
  ur_motors[3] = wb_robot_get_device("wrist_1_joint");
  ur_motors[4] = wb_robot_get_device("wrist_2_joint");
  ur_motors[5] = wb_robot_get_device("wrist_3_joint");
 
  WbDeviceTag Joint2 = wb_robot_get_device("shoulder_lift_joint_sensor");
  wb_position_sensor_enable(Joint2, TIME_STEP);
  WbDeviceTag Joint3 = wb_robot_get_device("shoulder_pan_joint_sensor");
  wb_position_sensor_enable(Joint3, TIME_STEP);
  WbDeviceTag Joint1 = wb_robot_get_device("elbow_joint_sensor");
  wb_position_sensor_enable(Joint1, TIME_STEP);
  WbDeviceTag Joint4 = wb_robot_get_device("wrist_1_joint_sensor");
  wb_position_sensor_enable(Joint4, TIME_STEP);
  WbDeviceTag Joint5 = wb_robot_get_device("wrist_2_joint_sensor");
  wb_position_sensor_enable(Joint5, TIME_STEP);
  WbDeviceTag Joint6 = wb_robot_get_device("wrist_3_joint_sensor");
  wb_position_sensor_enable(Joint6, TIME_STEP);
  
 for (i = 0; i < 6; ++i)
    wb_motor_set_velocity(ur_motors[i], speed);

// Main description
//Path and values
int pointer_l = 19; 
double JointPath[201][6] = { {-2.9793, -1.4756, -1.9510, -2.8565, 3.3039, 3.1416}, 
{-2.9622, -1.4466, -1.8705, -2.9661, 3.3209, 3.1416}, 
{-2.9447, -1.4274, -1.7687, -3.0871, 3.3385, 3.1416}, 
{-2.9353, -1.4229, -1.6853, -3.1751, 3.3479, 3.1416}, 
{-2.9329, -1.4264, -1.6094, -3.2474, 3.3503, 3.1416}, 
{-2.9399, -1.4353, -1.5476, -3.3003, 3.3433, 3.1416}, 
{-2.9636, -1.4496, -1.5015, -3.3321, 3.3196, 3.1416}, 
{-2.9827, -1.4576, -1.4929, -3.3327, 3.3005, 3.1416}, 
{-3.0254, -1.4726, -1.5005, -3.3101, 3.2578, 3.1416}, 
{-3.0789, -1.4918, -1.5281, -3.2633, 3.2043, 3.1416}, 
{-3.1521, -1.5240, -1.5739, -3.1852, 3.1310, 3.1416}, 
{-3.2167, -1.5607, -1.6123, -3.1103, 3.0665, 3.1416}, 
{-3.2811, -1.6060, -1.6431, -3.0341, 3.0021, 3.1416}, 
{-3.3426, -1.6576, -1.6615, -2.9641, 2.9406, 3.1416}, 
{-3.4098, -1.7218, -1.6634, -2.8980, 2.8734, 3.1416}, 
{-3.4454, -1.7578, -1.6534, -2.8719, 2.8377, 3.1416}, 
{-3.4843, -1.7969, -1.6302, -2.8561, 2.7989, 3.1416}, 
{-3.5227, -1.8344, -1.5926, -2.8562, 2.7604, 3.1416}, 
{-3.5631, -1.8723, -1.5361, -2.8748, 2.7201, 3.1416}, 
{-3.5933, -1.9007, -1.4824, -2.9002, 2.6899, 3.1416}}; 

  double sensor_value, joint_value;   
  setArray(JointPath[0]); //Initialize

// Main loop      
  while (wb_robot_step(TIME_STEP) != -1) {
  
  // Sensor errors 
  flag = false;
  for (i = 0; i < 6; ++i){
  switch (i)
  {
      case 0: sensor_value = wb_position_sensor_get_value(Joint1);
          break;
      case 1: sensor_value = wb_position_sensor_get_value(Joint2);
          break;
      case 2: sensor_value = wb_position_sensor_get_value(Joint3);
          break;
      case 3: sensor_value = wb_position_sensor_get_value(Joint4);
          break;
      case 4: sensor_value = wb_position_sensor_get_value(Joint5);
          break;
      case 5: sensor_value = wb_position_sensor_get_value(Joint6);
          break;
  }
  joint_value = target_positions[i];
  error[i] = joint_value-sensor_value;
  if (error[i]<0){
  error[i] = -error[i];
  }
  if (error[i]>error_margin){
  flag = true;
  }
  
  }

// Move or Set up next position   
  if (flag){
    for (i = 0; i < 6; ++i){
    wb_motor_set_position(ur_motors[i], target_positions[i]);
   }
  }
  if (!flag){
    if (pointer <= pointer_l){
    setArray(JointPath[pointer]); 
    printf("Next: %d. ", pointer);
    }
    for (i = 0; i < 6; ++i){
    joint_value = target_positions[i];
    printf("%f, ", joint_value);
    }        
    pointer++;
    }
  };

  wb_robot_cleanup();
  return 0;
}
